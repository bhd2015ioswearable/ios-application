//
//  PSDataManager.m
//  pulsation
//
//  Created by Stanislav Satunovsky on 6/11/15.
//  Copyright (c) 2015 DataArt. All rights reserved.
//

#import "PSDataManager.h"

const double kPSCaloriesPerStep = 0.025l;

static NSString * const kTimeKey = @"time";
static NSString * const kStepsKey = @"steps";

@interface PSDataManager ()

@property (strong, nonatomic) NSUserDefaults *storage;

@end

@implementation PSDataManager

#pragma mark - shared
+(PSDataManager *) sharedManager {
    static PSDataManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [PSDataManager new];
    });
    return sharedManager;
}

#pragma mark - storage
-(NSUserDefaults *) storage {
    if(!_storage) {
        self.storage = [[NSUserDefaults alloc] initWithSuiteName: @"group.com.dataart.Pulsation"];
    }
    return _storage;
}

#pragma mark - data
-(NSUInteger) steps {
    return [[self.storage objectForKey: kStepsKey] unsignedIntegerValue];
}

-(NSUInteger) calories {
    return self.steps * kPSCaloriesPerStep;
}

-(NSString *) time {
    NSDate *beginDate = [self.storage objectForKey: kTimeKey];
    
    if(!beginDate) {
        return @"00:00:00";
    }
    
    NSInteger time = -(NSInteger)[beginDate timeIntervalSinceNow];
     
    NSInteger seconds = time % 60;
     
    time -= seconds;
    time /= 60;
     
    NSInteger minutes = time % 60;
    time -= minutes;
    time /= 60;
     
    NSInteger hours = time % 24;
    
    NSString *timeString = [NSString stringWithFormat: @"%02ld:%02ld:%02ld", hours, minutes, seconds];
    
    return timeString;
}

-(void) storeWorkout: (NSDate *) beginTime steps: (NSUInteger) steps {
    [self.storage setObject: beginTime
                     forKey: kTimeKey];
    
    [self.storage setObject: @(steps)
                     forKey: kStepsKey];
    
    [self.storage synchronize];
}

@end
