//
//  PSDataManager.h
//  pulsation
//
//  Created by Stanislav Satunovsky on 6/11/15.
//  Copyright (c) 2015 DataArt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSDataManager : NSObject

+(PSDataManager *) sharedManager;

-(void) storeWorkout: (NSDate *) beginTime steps: (NSUInteger) steps;

-(NSUInteger) steps;
-(NSUInteger) calories;
-(NSString *) time;

@end
