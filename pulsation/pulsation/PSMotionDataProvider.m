//
//  PSMotionDataProvider.m
//  pulsation
//
//  Created by Stanislav Satunovsky on 6/11/15.
//  Copyright (c) 2015 DataArt. All rights reserved.
//

#import "PSMotionDataProvider.h"

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>

#import "PSDataManager.h"

const NSUInteger kPSStepsNotificationInterval = 100;

#define EMULATE_STEP_COUNT 1

@interface PSMotionDataProvider ()

@property (strong, nonatomic) CMPedometer *pedometer;

@property (strong, nonatomic) NSDate *beginDate;
@property (assign, atomic) BOOL isActive;

-(void) startPedometer;
-(void) stopPedometer;

@property (assign, nonatomic) NSUInteger steps;
@property (assign, nonatomic) NSUInteger lastNotificationSteps;

#if EMULATE_STEP_COUNT
-(void) emulateSteps;
#endif

@end

@implementation PSMotionDataProvider

#pragma mark - shared
+(PSMotionDataProvider *) sharedProvider {
    static PSMotionDataProvider *sharedProvider = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedProvider = [PSMotionDataProvider new];
    });
    return sharedProvider;
}

#pragma mark - convenience
+(BOOL) wakeUp {
    // just create a shared instance
    return (BOOL)[PSMotionDataProvider sharedProvider];
}

#pragma mark - pedometer
-(CMPedometer *) pedometer {
    if(![CMPedometer isStepCountingAvailable]) {
        NSLog(@"[!!!]: device doesn't support hardware step counting");
        return nil;
    }
    
    if(!_pedometer) {
        self.pedometer = [CMPedometer new];
    }
    
    return _pedometer;
}

#pragma mark - updates
-(void) setSteps: (NSUInteger) steps {
    _steps = steps;
    
    [[PSDataManager sharedManager] storeWorkout: self.beginDate
                                          steps: self.steps];
    
    if((self.steps - self.lastNotificationSteps) >= kPSStepsNotificationInterval) {
        self.lastNotificationSteps = self.steps;
        
        UILocalNotification *notification = [[UILocalNotification alloc] init];
        
        notification.alertBody = [NSString stringWithFormat: @"%lu Cal", [PSDataManager sharedManager].calories];
        notification.alertAction = @"progress";
        notification.category = @"pulsation";
        
        [[UIApplication sharedApplication] presentLocalNotificationNow: notification];
    }
}

-(void) setBeginDate: (NSDate *) beginDate {
    _beginDate = beginDate;
    
    [[PSDataManager sharedManager] storeWorkout: self.beginDate
                                          steps: self.steps];
}

#if EMULATE_STEP_COUNT
-(void) startPedometer {
    if(self.isActive) {
        return;
    }
    
    self.isActive = YES;
    
    self.beginDate = [NSDate date];
    
    self.steps = 0;
    self.lastNotificationSteps = 0;
    
    [self emulateSteps];
}

-(void) emulateSteps {
    self.steps = self.steps + arc4random() % 10 + 1;
    
    [self performSelector: @selector(emulateSteps)
               withObject: nil
               afterDelay: 1.0f];
}

#else

-(void) startPedometer {
    if(!self.pedometer) {
        return;
    }
    
    if(self.isActive) {
        return;
    }
    
    self.isActive = YES;
    
    self.beginDate = [NSDate date];
    
    self.steps = 0;
    self.lastNotificationSteps = 0;
    
    [self.pedometer startPedometerUpdatesFromDate: self.beginDate
                                      withHandler: ^(CMPedometerData *pedometerData, NSError *error) {
                                          if(self.isActive && !error) {
                                              NSLog(@"pedometer reports: %@ steps from %@ to %@", pedometerData.numberOfSteps, pedometerData.startDate, pedometerData.endDate);
                                          }
                                      }];
}

#endif

-(void) stopPedometer {
    self.isActive = NO;
    
    [self.pedometer stopPedometerUpdates];
    
#if EMULATE_STEP_COUNT
    [NSObject cancelPreviousPerformRequestsWithTarget: self];
#endif
}

#pragma mark - external
-(void) start {
    [self startPedometer];
}

-(void) stop {
    [self stopPedometer];
}

#pragma mark - dealloc
-(void) dealloc {
    [self stopPedometer];
}

@end
