//
//  PSWelcomeScreen.m
//  pulsation
//
//  Created by Stanislav Satunovsky on 6/11/15.
//  Copyright (c) 2015 DataArt. All rights reserved.
//

#import "PSWelcomeScreen.h"

#import <QuartzCore/QuartzCore.h>

@interface PSWelcomeScreen ()

@property (weak, nonatomic) IBOutlet UIView *bottomBarBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *centralButtonBackgroundView;

@end

@implementation PSWelcomeScreen

#pragma mark - view
-(void)viewDidLoad {
    [super viewDidLoad];
    
    // bottom bar style
    self.bottomBarBackgroundView.layer.cornerRadius = 16.0f;
    
    // central button background style
    CAShapeLayer *centralCircleMask = [CAShapeLayer new];
    centralCircleMask.path = CGPathCreateWithEllipseInRect(self.centralButtonBackgroundView.bounds, &CGAffineTransformIdentity);
    CGPathRelease(centralCircleMask.path);
    self.centralButtonBackgroundView.layer.mask = centralCircleMask;
}

@end
