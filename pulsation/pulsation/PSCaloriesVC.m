//
//  PSCaloriesVC.m
//  pulsation
//
//  Created by Stanislav Satunovsky on 6/12/15.
//  Copyright (c) 2015 DataArt. All rights reserved.
//

#import "PSCaloriesVC.h"

#import "PSMotionDataProvider.h"
#import "PSDataManager.h"

@interface PSCaloriesVC ()

@property (weak, nonatomic) IBOutlet UILabel *caloriesLabel;

-(void) updateCalories;

@end

@implementation PSCaloriesVC

#pragma mark - view
-(void) viewDidLoad {
    [super viewDidLoad];
    
    [self updateCalories];
}

#pragma mark - update
-(void) updateCalories {
    if([PSMotionDataProvider sharedProvider].isActive) {
        self.caloriesLabel.text = [NSString stringWithFormat: @"%lu", [PSDataManager sharedManager].calories];
    }
    
    [self performSelector: @selector(updateCalories)
               withObject: nil
               afterDelay: 1.0f];
}

@end
