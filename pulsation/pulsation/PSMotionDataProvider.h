//
//  PSMotionDataProvider.h
//  pulsation
//
//  Created by Stanislav Satunovsky on 6/11/15.
//  Copyright (c) 2015 DataArt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSMotionDataProvider : NSObject

+(PSMotionDataProvider *) sharedProvider;

+(BOOL) wakeUp;

@property (readonly, atomic) BOOL isActive;

-(void) start;
-(void) stop;

@end
