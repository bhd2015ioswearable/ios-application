//
//  PSVoucherVC.h
//  pulsation
//
//  Created by Dmitry Gnatuk on 6/13/15.
//  Copyright (c) 2015 DataArt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSVoucherVC : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *buttonVoucher;

@end
