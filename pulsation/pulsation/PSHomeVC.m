//
//  PSHomeVC.m
//  pulsation
//
//  Created by Stanislav Satunovsky on 6/11/15.
//  Copyright (c) 2015 DataArt. All rights reserved.
//

#import "PSHomeVC.h"

#import <QuartzCore/QuartzCore.h>

#import "PSMotionDataProvider.h"

#import "PSWorkoutVC.h"
#import "PSTimeVC.h"
#import "PSCaloriesVC.h"
#import "PSVoucherVC.h"

typedef enum PSHomeDisplayMode {
    PSHomeDisplayModeVoucher = 0,
    PSHomeDisplayModeWorkout,
    PSHomeDisplayModeTime,
    PSHomeDisplayModeCalories,
    PSHomeDisplayModeGo
} PSHomeDisplayMode;

@interface PSHomeVC ()

@property (weak, nonatomic) IBOutlet UIView *screenContainerView;

@property (weak, nonatomic) IBOutlet UIView *centralButtonBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *topBackgroundPanelRoundedView;
@property (weak, nonatomic) IBOutlet UIView *bottomBackgroundPanelRoundedView;

@property (weak, nonatomic) IBOutlet UIView *motivationView;

@property (weak, nonatomic) IBOutlet UIView *contentContainerView;

@property (weak, nonatomic) IBOutlet UIButton *workoutButton;
@property (weak, nonatomic) IBOutlet UIButton *timeButton;
@property (weak, nonatomic) IBOutlet UIButton *calButton;
@property (weak, nonatomic) IBOutlet UIButton *goButton;

@property (strong, nonatomic) PSWorkoutVC *workoutVC;
@property (strong, nonatomic) PSTimeVC *timeVC;
@property (strong, nonatomic) PSCaloriesVC *caloriesVC;
@property (strong, nonatomic) PSVoucherVC *voucherVC;

@property (assign, nonatomic) PSHomeDisplayMode displayMode;
@property (assign, nonatomic) BOOL expanded;

-(void) setExpanded: (BOOL) expanded animated: (BOOL) animated;

-(IBAction) largeStarAction: (id) sender;
-(IBAction) workoutAction: (id) sender;
-(IBAction) timeAction: (id) sender;
-(IBAction) caloriesAction: (id) sender;
-(IBAction) goAction: (id) sender;

@end

@implementation PSHomeVC

#pragma mark - view
-(void)viewDidLoad {
    [super viewDidLoad];
    
    // bar rounding
    self.topBackgroundPanelRoundedView.layer.cornerRadius = 16.0f;
    self.bottomBackgroundPanelRoundedView.layer.cornerRadius = 16.0f;
    
    // central button background style
    CAShapeLayer *centralCircleMask = [CAShapeLayer new];
    centralCircleMask.path = CGPathCreateWithEllipseInRect(self.centralButtonBackgroundView.bounds, &CGAffineTransformIdentity);
    CGPathRelease(centralCircleMask.path);
    self.centralButtonBackgroundView.layer.mask = centralCircleMask;
    
    self.expanded = YES;
    self.displayMode = PSHomeDisplayModeVoucher;
}

#pragma mark - lazy
-(PSWorkoutVC *) workoutVC {
    if(!_workoutVC) {
        self.workoutVC = [self.storyboard instantiateViewControllerWithIdentifier: @"workout"];
        
        _workoutVC.view.frame = self.contentContainerView.bounds;
    }
    return _workoutVC;
}

-(PSTimeVC *) timeVC {
    if(!_timeVC) {
        self.timeVC = [self.storyboard instantiateViewControllerWithIdentifier: @"time"];
        
        _timeVC.view.frame = self.contentContainerView.bounds;
    }
    return _timeVC;
}

-(PSCaloriesVC *) caloriesVC {
    if(!_caloriesVC) {
        self.caloriesVC = [self.storyboard instantiateViewControllerWithIdentifier: @"calories"];
        
        _caloriesVC.view.frame = self.contentContainerView.bounds;
    }
    return _caloriesVC;
}

-(PSVoucherVC *) voucherVC {
    if(!_voucherVC) {
        self.voucherVC = [self.storyboard instantiateViewControllerWithIdentifier: @"voucher"];
        
        _voucherVC.view.frame = self.contentContainerView.bounds;
        
        [_voucherVC.buttonVoucher addTarget:self
                                     action:@selector(goVoucher:)
                           forControlEvents:UIControlEventTouchUpInside];
    }
    return _voucherVC;
}

#pragma mark - expansion
-(void) setExpanded: (BOOL) expanded {
    [self setExpanded: expanded
             animated: NO];
}

-(void) setExpanded: (BOOL) expanded animated: (BOOL) animated {
    CGRect screenRect = self.view.bounds;
    CGRect offsetRect = CGRectOffset(screenRect, 0.0f, screenRect.size.height - 250.0f);
    
    void (^prepare)() = ^() {
        if(expanded) {
            self.motivationView.hidden = NO;
            self.motivationView.alpha = 1.0f;
            self.screenContainerView.frame = offsetRect;
        } else {
            self.motivationView.hidden = NO;
            self.motivationView.alpha = 0.0f;
            self.screenContainerView.frame = screenRect;
        }
    };
    
    void (^move)() = ^() {
        if(expanded) {
            self.screenContainerView.frame = screenRect;
            self.motivationView.alpha = 0.0f;
        } else {
            self.screenContainerView.frame = offsetRect;
            self.motivationView.alpha = 1.0f;
        }
    };
    
    void (^finalize)() = ^() {
        if(expanded) {
            self.motivationView.hidden = YES;
        }
        
        _expanded = expanded;
    };
    
    prepare();
    
    if(animated) {
        [UIView animateWithDuration: 1.0f
                         animations: ^{
                             move();
                         }
                         completion: ^(BOOL finished) {
                             finalize();
                         }];
    } else {
        move();
        finalize();
    }
}

#pragma mark - display mode
-(void) setDisplayMode: (PSHomeDisplayMode) displayMode {
    static BOOL firstTime = YES;
    
    if(!firstTime) {
        if(_displayMode == displayMode) {
            return;
        }
    }
    
    _displayMode = displayMode;
    
    self.workoutButton.selected = NO;
    self.timeButton.selected = NO;
    self.calButton.selected = NO;
    self.goButton.selected = NO;
    
    [self.contentContainerView.subviews enumerateObjectsUsingBlock: ^(UIView *view, NSUInteger idx, BOOL *stop) {
        [view removeFromSuperview];
    }];
    
    switch (displayMode) {
        default:
        case PSHomeDisplayModeWorkout: {
            [self.contentContainerView addSubview: self.workoutVC.view];
            
            self.workoutButton.selected = YES;
        }
            break;
        case PSHomeDisplayModeTime: {
            [self.contentContainerView addSubview: self.timeVC.view];
            
            self.timeButton.selected = YES;
        }
            break;
        case PSHomeDisplayModeCalories: {
            [self.contentContainerView addSubview: self.caloriesVC.view];
            
            self.calButton.selected = YES;
        }
            break;
            
        case PSHomeDisplayModeVoucher: {
            [self.contentContainerView addSubview: self.voucherVC.view];
            
            self.timeButton.selected = YES;
        }
            break;
    }
    
    [self.contentContainerView setNeedsLayout];
    
    firstTime = NO;
}

#pragma mark - actions
-(IBAction) largeStarAction: (id) sender {
    [self setExpanded: !self.expanded
             animated: YES];
}

-(IBAction) workoutAction: (id) sender {
    self.displayMode = PSHomeDisplayModeWorkout;
}

-(IBAction) timeAction: (id) sender {
    self.displayMode = PSHomeDisplayModeVoucher;
}

-(IBAction) caloriesAction: (id) sender {
    self.displayMode = PSHomeDisplayModeCalories;
}

-(IBAction) goAction: (id) sender {
    [[PSMotionDataProvider sharedProvider] start];
}

-(void) goVoucher:(UIButton *)sender {
    self.displayMode = PSHomeDisplayModeCalories;
}

@end
