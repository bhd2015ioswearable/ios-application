//
//  PSVoucherVC.m
//  pulsation
//
//  Created by Dmitry Gnatuk on 6/13/15.
//  Copyright (c) 2015 DataArt. All rights reserved.
//

#import "PSVoucherVC.h"

@interface PSVoucherVC ()

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIView *viewContent;

@end

@implementation PSVoucherVC

-(void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.scrollView.contentSize = self.viewContent.frame.size;
}

@end
