//
//  PSWorkoutVC.m
//  pulsation
//
//  Created by Stanislav Satunovsky on 6/12/15.
//  Copyright (c) 2015 DataArt. All rights reserved.
//

#import "PSWorkoutVC.h"

#import <QuartzCore/QuartzCore.h>

@interface PSWorkoutVC ()

@property (weak, nonatomic) IBOutletCollection(UIView) NSArray *buttonBackgroundViews;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIView *bubbleView;
@property (weak, nonatomic) IBOutlet UIView *bubbleBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *bubbleArrowView;
@property (weak, nonatomic) IBOutlet UILabel *bubbleLabel;

@property (weak, nonatomic) IBOutlet UISlider *rewardSlider;

-(void) updateBubble;

-(IBAction) sliderValueChanged: (id) sender;
-(IBAction) button1Action: (id) sender;
-(IBAction) button2Action: (id) sender;
-(IBAction) button3Action: (id) sender;

@end

@implementation PSWorkoutVC

#pragma mark - view
-(void) viewDidLoad {
    [super viewDidLoad];
    
    [self.buttonBackgroundViews enumerateObjectsUsingBlock: ^(UIView *view, NSUInteger idx, BOOL *stop) {
        view.layer.cornerRadius = 6.0f;
    }];
    
    self.bubbleBackgroundView.layer.cornerRadius = 6.0f;
    self.bubbleArrowView.transform = CGAffineTransformMakeRotation(M_PI_4);
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    
    [self updateBubble];
}

-(void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.scrollView.contentSize = self.contentView.frame.size;
    
    [self updateBubble];
}

#pragma mark - bubble
-(void) updateBubble {
    float value = self.rewardSlider.value / 100.0f;
    
    self.bubbleView.center = CGPointMake(floorf(self.rewardSlider.frame.origin.x + (self.rewardSlider.frame.size.width - 30.0f) * value + 15.0f),
                                         self.bubbleView.center.y);
    
    self.bubbleLabel.text = [NSString stringWithFormat: @"%.0f €", self.rewardSlider.value];
}

#pragma mark - action
-(IBAction) sliderValueChanged: (id) sender {
    [self updateBubble];
}

-(IBAction) button1Action: (id) sender {
   
}

-(IBAction) button2Action: (id) sender {
    
}

-(IBAction) button3Action: (id) sender {
    
}

@end
