//
//  AppDelegate.m
//  pulsation
//
//  Created by Stanislav Satunovsky on 6/11/15.
//  Copyright (c) 2015 DataArt. All rights reserved.
//

#import "AppDelegate.h"

#import "PSMotionDataProvider.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

-(BOOL) application: (UIApplication *) application didFinishLaunchingWithOptions: (NSDictionary *) launchOptions {
    if(![PSMotionDataProvider wakeUp]) {
        NSLog(@"[!!!]: unable to wake up motion data provider");
    }
    
    return YES;
}

-(void) application: (UIApplication *) application handleWatchKitExtensionRequest: (NSDictionary *) userInfo reply: (void (^)(NSDictionary *)) reply {
    [[PSMotionDataProvider sharedProvider] start];
}

@end
