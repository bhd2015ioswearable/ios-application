//
//  PSTimeVC.m
//  pulsation
//
//  Created by Stanislav Satunovsky on 6/12/15.
//  Copyright (c) 2015 DataArt. All rights reserved.
//

#import "PSTimeVC.h"

#import "PSMotionDataProvider.h"
#import "PSDataManager.h"

@interface PSTimeVC ()

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

-(void) updateTime;

@end

@implementation PSTimeVC

#pragma mark - view
-(void) viewDidLoad {
    [super viewDidLoad];
    
    [self updateTime];
}

#pragma mark - update
-(void) updateTime {
    if([PSMotionDataProvider sharedProvider].isActive) {
        self.timeLabel.text = [PSDataManager sharedManager].time;
    }
    
    [self performSelector: @selector(updateTime)
               withObject: nil
               afterDelay: 1.0f];
}

@end
