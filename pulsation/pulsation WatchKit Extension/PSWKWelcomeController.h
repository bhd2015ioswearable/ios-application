//
//  InterfaceController.h
//  pulsation WatchKit Extension
//
//  Created by Stanislav Satunovsky on 6/11/15.
//  Copyright (c) 2015 DataArt. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>

@interface PSWKWelcomeController : WKInterfaceController

@end
