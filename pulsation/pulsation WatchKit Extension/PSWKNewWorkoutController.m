//
//  PSWKNewWorkoutController.m
//  pulsation
//
//  Created by Stanislav Satunovsky on 6/12/15.
//  Copyright (c) 2015 DataArt. All rights reserved.
//

#import "PSWKNewWorkoutController.h"

@interface PSWKNewWorkoutController ()

-(IBAction) workout;

@end

@implementation PSWKNewWorkoutController

-(IBAction) workout {
    // open parent app & start workout
    [PSWKNewWorkoutController openParentApplication: @{}
                                              reply: nil];
    
    [self presentControllerWithName: @"workout"
                            context: nil];
}

@end
