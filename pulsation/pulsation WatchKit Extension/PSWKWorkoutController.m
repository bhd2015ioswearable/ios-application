//
//  PSWorkoutController.m
//  pulsation
//
//  Created by Stanislav Satunovsky on 6/12/15.
//  Copyright (c) 2015 DataArt. All rights reserved.
//

#import "PSWKWorkoutController.h"

#import "PSDataManager.h"

@interface PSWKWorkoutController ()

@property (weak, nonatomic) IBOutlet WKInterfaceLabel *timeLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *caloriesLabel;

-(void) update;

@end

@implementation PSWKWorkoutController

#pragma mark - lifecycle
-(void) willActivate {
    [self update];
}

-(void) didDeactivate {
    [NSObject cancelPreviousPerformRequestsWithTarget: self];
}

#pragma mark - update
-(void) update {
    self.timeLabel.text = [PSDataManager sharedManager].time;
    self.caloriesLabel.text = [NSString stringWithFormat: @"%lu", [PSDataManager sharedManager].calories];
    
    [self performSelector: @selector(update)
               withObject: nil
               afterDelay: 1.0f];
}

@end
